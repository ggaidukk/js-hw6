function createNewUser() {
  let name = prompt("Вкажіть ваше ім'я");
  let surname = prompt("Вкажіть ваше прізвище");
  let birthday = prompt("Ваша дата народження");
  let newUser = {
    firstName: name,
    lastName: surname,
  };
  function getLogin() {
    return newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase();
  }
  console.log(getLogin());
  function getAge() {
    let day = parseInt(birthday.substring(0, 2));
    let month = parseInt(birthday.substring(3, 5));
    let year = parseInt(birthday.substring(6, 10));

    let today = new Date();
    let birthDate = new Date(year, month - 1, day);
    let age = today.getFullYear() - birthDate.getFullYear();
    let i = today.getMonth() - birthDate.getMonth();
    if (i < 0 || (i === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }
  console.log(getAge());
  function getPassword() {
    let year = parseInt(birthday.substring(6, 10));
    return `${getLogin()}${year}`;
  }
  console.log(getPassword());
}
createNewUser();
